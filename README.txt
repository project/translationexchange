
Module: Translation Exchange
Author: Translation Exchange <https://translationexchange.com>


Description
===========
Adds Translation Exchange localization tools to your Drupal website.


Requirements
============

* Please create an account and get a project key from Translation Exchange (https://translationexchange.com).


Installation
============
Copy the 'translationexchange' module directory in to your Drupal sites/all/modules directory.


Usage
=====
In the settings page enter your Translation Exchange project key.

The plugin will add the necessary JS files and provide production and translation modes.

Read more about how to use the plugin here: https://docs.translationexchange.com/drupal
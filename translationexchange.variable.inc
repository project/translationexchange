<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 */
function translationexchange_variable_info($options) {
  $variables['trex_project_key'] = array(
    'type' => 'string',
    'title' => t('Project Key', array(), $options),
    'default' => '',
    'description' => t('Put here your localize project key. Create your project on https://translationexchange.com.'),
    'required' => TRUE,
    'group' => 'translationexchange',
    'localize' => TRUE,
    'multidomain' => TRUE,
    'validate callback' => 'translationexchange_validate_trex_project_key',
  );
  $variables['trex_disable_admin_translation'] = array(
    'type' => 'boolean',
    'title' => t('Disable on admin paths', array(), $options),
    'default' => TRUE,
    'group' => 'translationexchange',
    'localize' => FALSE,
  );

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function translationexchange_variable_group_info() {
  $groups['translationexchange'] = array(
    'title' => t('Localizejs'),
    'description' => t('Configure your project key to integrate Translation Exchange into your Drupal site.'),
    'access' => 'administer translationexchange',
    'path' => array('admin/config/system/translationexchange'),
  );

  return $groups;
}

/**
 * Validate translationexchange key variable.
 */
function translationexchange_validate_trex_project_key($variable) {

}

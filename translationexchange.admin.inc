<?php

/**
 * @file
 * Administrative page callbacks for the translationexchange module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function translationexchange_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['account']['trex_project_key'] = array(
    '#title' => t('Project Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('trex_project_key', ''),
    '#size' => 100,
    '#maxlength' => 100,
    '#required' => TRUE,
    '#description' => t('Please copy and paste here your Translation Exchange project key.'),
  );
  $form['trex_disable_admin_translation'] = array(
    '#title' => t('Disable Translation Exchange on administrative pages.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('trex_disable_admin_translation', TRUE),
  );

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function translationexchange_admin_settings_form_validate($form, &$form_state) {

}

/**
 * Implements _form_submit().
 */
function translationexchange_admin_settings_form_submit($form, &$form_state) {

}
